import {SocketConnection} from "./SocketConnection";

export class RTCPeerConnectionService {
    private peerConnection:any;

    private socketConnection:WebSocket;

    private userIdentifier:number;

    private peerConnectionConfig:any = {'iceServers': [{'url': 'stun:stun.services.mozilla.com'}, {'url': 'stun:stun.l.google.com:19302'}]};

    constructor(socket:SocketConnection, userIdentifier:number) {
        this.socketConnection = SocketConnection.getInstance();
        this.peerConnection = new RTCPeerConnection(this.peerConnectionConfig);
        this.peerConnection.onicecandidate = this.gotIceCandidate.bind(this);
        this.peerConnection.onaddstream = this.gotRemoteStream.bind(this);
        this.socketConnection.addHandler('onmessage', this.connectToPeers.bind(this));
        this.userIdentifier = userIdentifier;
    }

    private connectToPeers(message){
        var signal = JSON.parse(message.data);
        if(signal.sdp) {
            this.peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp), () => {
                console.log('121121212');
                if(signal.sdp.type == 'offer') {
                    console.log('OFFER RECEIVED');
                    this.peerConnection.createAnswer(this.gotDescription.bind(this), (error) => {
                        throw new Error(error)
                    });
                }
            });
        }
        else if(signal.ice) {
            console.log(signal);
            this.peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice));
        }
    }

    public createOffer() {
         this.peerConnection.createOffer(gotDescription, createOfferError);
    }

    public gotDescription(description) {
        console.log('got description', description);
        var self = this;
        this.peerConnection.setLocalDescription(description, () => {
            this.socketConnection.send(JSON.stringify({'sdp': description, "identifier": this.userIdentifier}));
        }, () => {console.log('set description error')});
    }

    public gotIceCandidate(event) {
        if(event.candidate != null) {
            this.socketConnection.send(JSON.stringify({'ice': event.candidate, "identifier": this.userIdentifier}));
        }
    }

    public gotRemoteStream(event) {
        console.log('got remote stream');
        var video = document.getElementById('video');
        video.src = window.URL.createObjectURL(event.stream);
        // return 
    }

    public addStream(stream:MediaStream) {
        this.peerConnection.addStream(stream);
    }

    public start()
    {
        this.peerConnection.createOffer(this.gotDescription.bind(this), (error) => {
            throw new Error(error);
        });
    }
}