import {UserMedia} from "./UserMedia";
import {RTCPeerConnectionService} from "./RTCPeerConnectionService";
import {SocketConnection} from "./SocketConnection";

class Main {

    private userMedia:UserMedia;
    private userCameraStream:MediaStream;
    private socketConnection:any;
    private peerConnection:RTCPeerConnectionService;
    private userIdentifier:number;

    public constructor() 
    {
        this.userMedia = new UserMedia;
        this.userIdentifier = Math.floor(Math.random() * (99999 - 0 + 1)) + 0;
        this.requestUserCameraStream();
        this.socketConnection = new SocketConnection('ws://localhost:9999/websocket');
        this.socketConnection.addHandler('onopen', () => {
            this.socketConnection.send(JSON.stringify({identifier: this.userIdentifier}));
        }); 
        this.peerConnection = new RTCPeerConnectionService(this.socketConnection, this.userIdentifier);
        var startButton = document.getElementById('call').addEventListener('click', this.peerConnection.start.bind(this.peerConnection));
    }

    public requestUserCameraStream()
    {
        var self = this;
        this.userMedia.getCameraStream().then((stream) => {
            self.peerConnection.addStream(stream);
        });
    }
}

let main = new Main();