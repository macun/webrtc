export class SocketConnection {

    private static _instance;

    private socketConnection:WebSocket;

    constructor(socketUrl:string)
    {
        if(SocketConnection._instance) {
            throw new Error("Error: Instantiation failed: Use SingletonClass.getInstance() instead of new.");
        }
        this.socketConnection = new WebSocket(socketUrl);

        SocketConnection._instance = this;
    }

    public static getInstance()
    {
        return SocketConnection._instance;
    }

    public addHandler(handlerName:string, handler:Function)
    {
        this.socketConnection[handlerName] = handler;
    }

    public send(message:string)
    {
        this.socketConnection.send(message);
    }
}