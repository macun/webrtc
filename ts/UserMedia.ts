export class UserMedia {

    public isAvailable = false;
    
    public constructor() {
        if(!this.isUserMediaAvailable()) {
            throw new Error('User Media API not available on this browser');
        }
        this.isAvailable = this.isUserMediaAvailable();
    }

    public isUserMediaAvailable()
    {
        return !! ( navigator.getUserMedia = navigator.mediaDevices || navigator.mediaDevices.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia);
    }

    public getCameraStream()
    {
        return new Promise((resolve, reject) => {
            navigator.mediaDevices.getUserMedia({ audio: true, video: true }).then((stream) => {
                resolve(stream);
            }, (error) => {
                throw new Error(error);
            });
        });
    }
}