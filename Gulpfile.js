var gulp = require('gulp');
var ts = require('gulp-typescript');
var clean = require('gulp-clean');
var webpack = require('webpack-stream');
var replace = require('gulp-html-replace');
var webserver = require('gulp-webserver');
var path = require('path');


var outputHash;

gulp.task('clean', function(){
    gulp.src("js")
        .pipe(clean());
    gulp.src("dest")
        .pipe(clean());
});

gulp.task('serve', ['index'], function() {
    gulp.src('dest')
        .pipe(webserver({
            livereload: true,
            // fallback: 'index.html',
        }));
});


gulp.task('ts', ['clean'], function(){
    return gulp.src('./ts/**/*.ts')
        .pipe(ts({
            module: "amd"
        }))
        .pipe(gulp.dest('./js'))
});

gulp.task('webpack', ['clean'], function(){
    return gulp.src('./ts/**/*.ts')
        .pipe(webpack(webpackConfig, null, function(err, stats){
            outputHash = stats.compilation.hash;
        }))
        .pipe(gulp.dest('dest'));
});

gulp.task('index', ['webpack'], function(webpack) {
    return gulp.src('ts/index.html')
        .pipe(gulp.dest('dest/'))
        .pipe(replace({
            'js':  outputHash+'.js'
        }))
        .pipe(gulp.dest('dest/'));
});

gulp.task('default', ['webpack', 'index']);
gulp.task('watch', ['default', 'serve'], function(){
    gulp.watch(['./ts/**/*.ts', './ts/index.html'], ['webpack', 'index', 'default']);
});

var webpackConfig = {
    devtool: 'source-map',
    resolve: {
        root: [path.join(__dirname, 'node_modules')],
        extensions: ['', '.ts', '.js'],
        modulesDirectories: [path.join(__dirname, 'node_modules')],
    },
    resolveModules: {
        modulesDirectories: [path.join(__dirname, 'node_modules')]
    },
    module: {
        loaders: [
            { test: /\.tsx?$/, loader: "ts-loader?" + JSON.stringify({
                configFileName: path.join(__dirname, './tsconfig.json'),
                transpileOnly: true
            }) }
        ]
    }
}