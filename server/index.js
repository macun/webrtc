var http = require('http');
var sockjs = require('sockjs');
var queryString = require('querystring');
var url = require('url');


var identifiedClients = {};
var echo = sockjs.createServer({ sockjs_url: 'http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js' });

var server = http.createServer();
echo.installHandlers(server);
server.listen(9999);

// var WebSocketServer = require('ws').Server;

// var wss = new WebSocketServer({port: 3434});

var onConnection = function(conn) {
    var username = queryString.parse(url.parse(conn._session.ws.url).query).username;
    // if(!username) {
    //     conn.close();
    // }
    identifiedClients[conn.id] = conn._session;
    identifiedClients[conn.id][username] = username;
    conn.on('data', onData.bind(conn));
    conn.on('close', onClose.bind(conn))
};

var onData = function(message) {
    broadcast(message, this);
};

var onClose = function() {
    delete identifiedClients[this.id];
    broadcast(JSON.stringify({remove: this.id}));
}

var broadcast = function(data, connection) {
    var identifier = JSON.parse(data);
    for(var i in identifiedClients) {
        if(i !== identifier){
            identifiedClients[i].send(data);
        }
    }
};
echo.on('connection', onConnection);
echo.on('close', onClose);